package off;

public class Rappel {
	private String natureJuridiqueDuRappel;
    private String categorieDeProduit;
    private String sousCategorieDeProduit;
    private String nomDeLaMarqueDuProduit;
    private String nomsDesModelesOuReferences;
    private String identificationDesProduits;
    private String informationsComplementaires;
    private String zoneGeographiqueDeVente;
    private String distributeurs;
    private String motifDuRappel;
    private String risquesEncourusParLeConsommateur;
    private String liensVersLesImages;
    private String lienVersLaFicheRappel;
    private String rappelguid;

    public Rappel(String natureJuridiqueDuRappel, String categorieDeProduit, String sousCategorieDeProduit, String nomDeLaMarqueDuProduit, String nomsDesModelesOuReferences, String identificationDesProduits, String informationsComplementaires, String zoneGeographiqueDeVente, String distributeurs, String motifDuRappel, String risquesEncourusParLeConsommateur, String liensVersLesImages, String lienVersLaFicheRappel, String rappelguid) {
        this.natureJuridiqueDuRappel = natureJuridiqueDuRappel;
        this.categorieDeProduit = categorieDeProduit;
        this.sousCategorieDeProduit = sousCategorieDeProduit;
        this.nomDeLaMarqueDuProduit = nomDeLaMarqueDuProduit;
        this.nomsDesModelesOuReferences = nomsDesModelesOuReferences;
        this.identificationDesProduits = identificationDesProduits;
        this.informationsComplementaires = informationsComplementaires;
        this.zoneGeographiqueDeVente = zoneGeographiqueDeVente;
        this.distributeurs = distributeurs;
        this.motifDuRappel = motifDuRappel;
        this.risquesEncourusParLeConsommateur = risquesEncourusParLeConsommateur;
        this.liensVersLesImages = liensVersLesImages;
        this.lienVersLaFicheRappel = lienVersLaFicheRappel;
    }
    
    public Rappel(String noms_des_modeles_ou_references, String sous_categorie_de_produit, String rappelguid, String liens_vers_les_images) {
    	this.nomsDesModelesOuReferences = noms_des_modeles_ou_references;
        this.sousCategorieDeProduit = sous_categorie_de_produit;
        this.rappelguid = rappelguid;
        this.liensVersLesImages = liens_vers_les_images;
    }
    
    public String getNatureJuridiqueDuRappel() {
		return natureJuridiqueDuRappel;
	}

	public void setNatureJuridiqueDuRappel(String natureJuridiqueDuRappel) {
		this.natureJuridiqueDuRappel = natureJuridiqueDuRappel;
	}

	public String getCategorieDeProduit() {
		return categorieDeProduit;
	}

	public void setCategorieDeProduit(String categorieDeProduit) {
		this.categorieDeProduit = categorieDeProduit;
	}

	public String getSousCategorieDeProduit() {
		return sousCategorieDeProduit;
	}

	public void setSousCategorieDeProduit(String sousCategorieDeProduit) {
		this.sousCategorieDeProduit = sousCategorieDeProduit;
	}

	public String getNomDeLaMarqueDuProduit() {
		return nomDeLaMarqueDuProduit;
	}

	public void setNomDeLaMarqueDuProduit(String nomDeLaMarqueDuProduit) {
		this.nomDeLaMarqueDuProduit = nomDeLaMarqueDuProduit;
	}

	public String getNomsDesModelesOuReferences() {
		return nomsDesModelesOuReferences;
	}

	public void setNomsDesModelesOuReferences(String nomsDesModelesOuReferences) {
		this.nomsDesModelesOuReferences = nomsDesModelesOuReferences;
	}

	public String getIdentificationDesProduits() {
		return identificationDesProduits;
	}

	public void setIdentificationDesProduits(String identificationDesProduits) {
		this.identificationDesProduits = identificationDesProduits;
	}

	public String getInformationsComplementaires() {
		return informationsComplementaires;
	}

	public void setInformationsComplementaires(String informationsComplementaires) {
		this.informationsComplementaires = informationsComplementaires;
	}

	public String getZoneGeographiqueDeVente() {
		return zoneGeographiqueDeVente;
	}

	public void setZoneGeographiqueDeVente(String zoneGeographiqueDeVente) {
		this.zoneGeographiqueDeVente = zoneGeographiqueDeVente;
	}

	public String getDistributeurs() {
		return distributeurs;
	}

	public void setDistributeurs(String distributeurs) {
		this.distributeurs = distributeurs;
	}

	public String getMotifDuRappel() {
		return motifDuRappel;
	}

	public void setMotifDuRappel(String motifDuRappel) {
		this.motifDuRappel = motifDuRappel;
	}

	public String getRisquesEncourusParLeConsommateur() {
		return risquesEncourusParLeConsommateur;
	}

	public void setRisquesEncourusParLeConsommateur(String risquesEncourusParLeConsommateur) {
		this.risquesEncourusParLeConsommateur = risquesEncourusParLeConsommateur;
	}

	public String getLiensVersLesImages() {
		return liensVersLesImages;
	}

	public void setLiensVersLesImages(String liensVersLesImages) {
		this.liensVersLesImages = liensVersLesImages;
	}

	public String getLienVersLaFicheRappel() {
		return lienVersLaFicheRappel;
	}

	public void setLienVersLaFicheRappel(String lienVersLaFicheRappel) {
		this.lienVersLaFicheRappel = lienVersLaFicheRappel;
	}

	public String getRappelguid() {
		return rappelguid;
	}

	public void setRappelguid(String rappelguid) {
		this.rappelguid = rappelguid;
	}
   
}

