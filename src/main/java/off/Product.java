package off;

public class Product {
	private int id;
    private String code;
    private String url;
    private String productName;
    private String brands;
    private String brandsTags;
    private String categoriesFr;
    private String countriesFr;
    private String ingredientsTags;
    private String allergens;
    private String additivesFr;
    private String nutriscoreGrade;
    private String foodGroupsFr;
    private String imageUrl;
    private String imageSmallUrl;
    private String energyKcal100g;
    private String energy100g;
    private String fat100g;
    private String saturatedFat100g;
    private String carbohydrates100g;
    private String sugars100g;
    private String fiber100g;
    private String proteins100g;
    private String salt100g;
    private String sodium100g;

    public Product(int id, String code, String url, String productName, String brands, String brandsTags, String categoriesFr, String countriesFr, String ingredientsTags, String allergens, String additivesFr, String nutriscoreGrade, String foodGroupsFr, String imageUrl, String imageSmallUrl, String energyKcal100g, String energy100g, String fat100g, String saturatedFat100g, String carbohydrates100g, String sugars100g, String fiber100g, String proteins100g, String salt100g, String sodium100g) {
    	this.id = id;
    	this.code = code;
        this.url = url;
        this.productName = productName;
        this.brands = brands;
        this.brandsTags = brandsTags;
        this.categoriesFr = categoriesFr;
        this.countriesFr = countriesFr;
        this.ingredientsTags = ingredientsTags;
        this.allergens = allergens;
        this.additivesFr = additivesFr;
        this.nutriscoreGrade = nutriscoreGrade;
        this.foodGroupsFr = foodGroupsFr;
        this.imageUrl = imageUrl;
        this.imageSmallUrl = imageSmallUrl;
        this.energyKcal100g = energyKcal100g;
        this.energy100g = energy100g;
        this.fat100g = fat100g;
        this.saturatedFat100g = saturatedFat100g;
        this.carbohydrates100g = carbohydrates100g;
        this.sugars100g = sugars100g;
        this.fiber100g = fiber100g;
        this.proteins100g = proteins100g;
        this.salt100g = salt100g;
        this.sodium100g = sodium100g;
    }
    
    public Product(String code, String productName, String nutriscoreGrade, String imageSmallUrl) {
        this.code = code;
        this.productName = productName;
    	this.nutriscoreGrade=nutriscoreGrade;
        this.imageSmallUrl = imageSmallUrl;
    }
    
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBrands() {
		return brands;
	}

	public void setBrands(String brands) {
		this.brands = brands;
	}

	public String getBrandsTags() {
		return brandsTags;
	}

	public void setBrandsTags(String brandsTags) {
		this.brandsTags = brandsTags;
	}

	public String getCategoriesFr() {
		return categoriesFr;
	}

	public void setCategoriesFr(String categoriesFr) {
		this.categoriesFr = categoriesFr;
	}

	public String getCountriesFr() {
		return countriesFr;
	}

	public void setCountriesFr(String countriesFr) {
		this.countriesFr = countriesFr;
	}

	public String getIngredientsTags() {
		return ingredientsTags;
	}

	public void setIngredientsTags(String ingredientsTags) {
		this.ingredientsTags = ingredientsTags;
	}

	public String getAllergens() {
		return allergens;
	}

	public void setAllergens(String allergens) {
		this.allergens = allergens;
	}

	public String getAdditivesFr() {
		return additivesFr;
	}

	public void setAdditivesFr(String additivesFr) {
		this.additivesFr = additivesFr;
	}

	public String getNutriscoreGrade() {
		return nutriscoreGrade;
	}

	public void setNutriscoreGrade(String nutriscoreGrade) {
		this.nutriscoreGrade = nutriscoreGrade;
	}

	public String getFoodGroupsFr() {
		return foodGroupsFr;
	}

	public void setFoodGroupsFr(String foodGroupsFr) {
		this.foodGroupsFr = foodGroupsFr;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageSmallUrl() {
		return imageSmallUrl;
	}

	public void setImageSmallUrl(String imageSmallUrl) {
		this.imageSmallUrl = imageSmallUrl;
	}

	public String getEnergyKcal100g() {
		return energyKcal100g;
	}

	public void setEnergyKcal100g(String energyKcal100g) {
		this.energyKcal100g = energyKcal100g;
	}

	public String getEnergy100g() {
		return energy100g;
	}

	public void setEnergy100g(String energy100g) {
		this.energy100g = energy100g;
	}

	public String getFat100g() {
		return fat100g;
	}

	public void setFat100g(String fat100g) {
		this.fat100g = fat100g;
	}

	public String getSaturatedFat100g() {
		return saturatedFat100g;
	}

	public void setSaturatedFat100g(String saturatedFat100g) {
		this.saturatedFat100g = saturatedFat100g;
	}

	public String getCarbohydrates100g() {
		return carbohydrates100g;
	}

	public void setCarbohydrates100g(String carbohydrates100g) {
		this.carbohydrates100g = carbohydrates100g;
	}

	public String getSugars100g() {
		return sugars100g;
	}

	public void setSugars100g(String sugars100g) {
		this.sugars100g = sugars100g;
	}

	public String getFiber100g() {
		return fiber100g;
	}

	public void setFiber100g(String fiber100g) {
		this.fiber100g = fiber100g;
	}

	public String getProteins100g() {
		return proteins100g;
	}

	public void setProteins100g(String proteins100g) {
		this.proteins100g = proteins100g;
	}

	public String getSalt100g() {
		return salt100g;
	}

	public void setSalt100g(String salt100g) {
		this.salt100g = salt100g;
	}

	public String getSodium100g() {
		return sodium100g;
	}

	public void setSodium100g(String sodium100g) {
		this.sodium100g = sodium100g;
	}
}

