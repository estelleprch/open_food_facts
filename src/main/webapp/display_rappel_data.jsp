<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="off.Product" %>
<%@ page import="off.Rappel" %>
<%@ include file="connection.jsp" %>


<%
	//create list
	List<Rappel> listRappelData = new ArrayList<Rappel>();
	
	//Execute a SELECT statement 
	Statement stmt = conn.createStatement();
	String codeSting = session.getAttribute("codeint").toString();
	
	String sql = "select * from rappel where rappelguid='" + codeSting + "'";
	
	ResultSet rs = stmt.executeQuery(sql);
	
	// Process the results and get all valuable data
	while (rs.next()) {
		String natureJuridiqueDuRappel = rs.getString("nature_juridique_du_rappel");
		String categorieDeProduit = rs.getString("categorie_de_produit");
		String sousCategorieDeProduit = rs.getString("sous_categorie_de_produit");
		String nomDeLaMarqueDuProduit = rs.getString("nom_de_la_marque_du_produit");
		String nomsDesModelesOuReferences = rs.getString("noms_des_modeles_ou_references");
		String identificationDesProduits = rs.getString("identification_des_produits");
		String informationsComplementaires = rs.getString("informations_complementaires");
		String zoneGeographiqueDeVente = rs.getString("zone_geographique_de_vente");
		String distributeurs = rs.getString("distributeurs");
		String motifDuRappel = rs.getString("motif_du_rappel");
		String risquesEncourusParLeConsommateur = rs.getString("risques_encourus_par_le_consommateur");
		String liensVersLesImages = rs.getString("liens_vers_les_images");
		String lienVersLaFicheRappel = rs.getString("lien_vers_la_fiche_rappel");
		String rappelguid = rs.getString("rappelguid");
		
		listRappelData.add(new Rappel(natureJuridiqueDuRappel,categorieDeProduit,sousCategorieDeProduit,nomDeLaMarqueDuProduit,nomsDesModelesOuReferences,identificationDesProduits,informationsComplementaires,zoneGeographiqueDeVente,distributeurs,motifDuRappel,risquesEncourusParLeConsommateur,liensVersLesImages,lienVersLaFicheRappel,rappelguid));
	}
%>	