<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.List" %>

<%@ page import="javax.servlet.http.Cookie" %>

<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
	    <title>Information du rappel</title>
	
	    <style>
	        
	        .card_space {
	            margin-left: 10px;
	            margin-right: 10px;
	            margin-top: 10px;
	            margin-bottom: 10px;
	        }
	        
	        .filter_part {
	        	width: 10%;
	        	float: left;
	        }
	        
	        .list_part {
	        	width: 90%;
	        	float: right;
	        }
	
	        .nav_space {
	             margin: auto;
				 width: 50%;
				 padding: 10px;
	        }
	        
	        .card_align {
	        	vertical-align: top;
	        }
	        
	    </style>
	</head>
	<body>
		<%@include file="header.jsp" %>
		<%
		String imported_code = request.getParameter("code");
		if(imported_code==null)
			imported_code="1";

		session.setAttribute("codeint", imported_code);
		%>
		<%@ include file="display_rappel_data.jsp" %>
		
		
		
		
		<div class="card mb-3">
  <%if(listRappelData.get(0).getLiensVersLesImages()==null || listRappelData.get(0).getLiensVersLesImages().equals("")){%>
			<img alt="rappel_picture" width="500" height="500" src="foodLogo.png">
		<%} else {%>
			<img alt="rappel_picture" width="500" height="500" src="<%=listRappelData.get(0).getLiensVersLesImages()%>">
		<%}%>
		
  
	
  
  <ul class="list-group list-group-flush">
  <li class="list-group-item"><h4>Nature juridique du rappel :</h4> 
		<p><%=listRappelData.get(0).getNatureJuridiqueDuRappel()%></p>
		</li>
  <li class="list-group-item"><h4>Catégorie du rappel :</h4> 
		<p><%=listRappelData.get(0).getCategorieDeProduit()%></p></li>
  <li class="list-group-item"><h4>Sous catégorie du rappel :</h4>
		<p><%=listRappelData.get(0).getSousCategorieDeProduit()%></p></li>
  <li class="list-group-item"><h4>Marque du produit :</h4>
		<p><%=listRappelData.get(0).getNomDeLaMarqueDuProduit()%></p></li>
  <li class="list-group-item"><h4>Modele du produit :</h4>
		<p><%=listRappelData.get(0).getNomsDesModelesOuReferences()%></p></li>
	 <li class="list-group-item"><h4>Identification du produits :</h4>
		<p><%=listRappelData.get(0).getIdentificationDesProduits()%></p></li>
	<li class="list-group-item"><h4>Informations complémentaires :</h4>
		<p><%=listRappelData.get(0).getInformationsComplementaires()%></p></li>
	<li class="list-group-item"><h4>Zone géographique de vente :</h4>
		<p><%=listRappelData.get(0).getZoneGeographiqueDeVente()%></p></li>
	<li class="list-group-item"><h4>Distributeurs :</h4>
		<p> <%=listRappelData.get(0).getDistributeurs()%></p></li>
	<li class="list-group-item"><h4>Motif du rappel :</h4>
		<p><%=listRappelData.get(0).getMotifDuRappel()%></p></li>
	<li class="list-group-item"><h4>Risques Encourus Par Le Consommateur :</h4>
		<p><%=listRappelData.get(0).getRisquesEncourusParLeConsommateur()%></p></li>
	<li class="list-group-item"><h4>Fiche produit :</h4>
		<p><%=listRappelData.get(0).getLienVersLaFicheRappel()%></p></li>
</ul>
</div>
		
	<%@include file="footer.jsp" %>
	</body>
	</html>