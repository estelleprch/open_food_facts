<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Open Food Facts</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

</head>
<body>
<%@include file="header.jsp" %>
<div class="container">
<img src="foodLogo.png" style="width:250px; height:auto; margin-top: 30px" class="d-block mx-auto"  alt="food logo">
<div class="mx-auto text-center text-justify" style="width: 800px">
La malbouffe est devenue une tendance grandissante dans notre société moderne. 
Les aliments transformés riches en matière grasses, en sucre et en sel sont de plus en plus présents dans notre alimentation.
Les fast-foods, les snacks sucrés et salés, les boissons gazeuses et les plats préparés sont 
devenus des choix de repas courants pour de nombreuses personnes. Cette tendance a des conséquences 
négatives sur la santé des individus, entraînant une augmentation des maladies liées à l'alimentation, 
telles que l'obésité, le diabète, les maladies cardiaques et certains types de cancer. Il est donc important 
de sensibiliser le public 
sur les effets néfastes de la malbouffe et de promouvoir une alimentation saine et équilibrée.

<br/>
<br/>
<br/>
Le site web présentée vise à recenser les produits alimentaires du monde entier afin de 
fournir aux utilisateurs des informations détaillées sur ce qu'ils consomment. Cette application s'adresse 
aux personnes qui s'inquiètent de ce qu'elles mangent et qui cherchent à adopter une alimentation saine et équilibrée. 
L'application permettra aux utilisateurs d’entrer le nom d’un produit alimentaire présent dans une base de données 
afin d’obtenir des informations telles que la liste des ingrédients, les valeurs nutritionnelles, les allergènes et 
les additifs alimentaires. De plus, ils seront informés lors d’un rappel 
conso d’un produit. Cette application est un outil 
utile pour aider les gens à faire des choix alimentaires éclairés et à éviter la malbouffe.
</div>
</div>

<%@include file="footer.jsp" %>
</body>

</html>