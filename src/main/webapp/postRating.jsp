<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="off.Product" %>
<%@ page import="java.util.concurrent.ThreadLocalRandom" %>
<%@ include file="connection.jsp" %>

<%
	// preparing request
	PreparedStatement stmt = null;
	String sql = null;
	ResultSet rs = null;
	
	// get rating
	String rating = request.getParameter("rating");
	String code = request.getParameter("code");
	if(rating!=null) {
		if(rating.equals("1") || rating.equals("2") || rating.equals("3") || rating.equals("4") ||rating.equals("5") ){
			
	    	// Pr�parer et ex�cuter la requ�te SQL
		    sql = "SELECT id FROM product WHERE code = ?";
		    stmt = conn.prepareStatement(sql);
		    stmt.setString(1, code);
		    rs = stmt.executeQuery();
		
		    // R�cup�rer l'id du produit
		    int id_product = -1;
		    if (rs.next()) {
		        id_product = rs.getInt("id");
		    }
		    System.out.print(id_product);
		 	// Pr�parer et ex�cuter la requ�te SQL
		    sql = "INSERT INTO rating (note, id_product) VALUES (?, ?)";
		    stmt = conn.prepareStatement(sql);
		    stmt.setDouble(1, Double.parseDouble(rating));
		    stmt.setInt(2, id_product);
		    stmt.executeUpdate();
		}
	}
	
	if(code!=null) {
		response.sendRedirect("uniq_product.jsp?code="+code);	
	} else {
		response.sendRedirect("product_list.jsp");	
	}
%>