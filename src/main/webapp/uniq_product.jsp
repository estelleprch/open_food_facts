<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.List" %>
<%@ page import="java.math.RoundingMode" %>
<%@ page import="java.text.DecimalFormat" %>

<%@ page import="javax.servlet.http.Cookie" %>

<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
	    <title>Information du produit</title>
	
	    <style>
	        
	        .card_space {
	            margin-left: 10px;
	            margin-right: 10px;
	            margin-top: 10px;
	            margin-bottom: 10px;
	        }
	        
	        .filter_part {
	        	width: 10%;
	        	float: left;
	        }
	        
	        .list_part {
	        	width: 90%;
	        	float: right;
	        }
	
	        .nav_space {
	             margin: auto;
				 width: 50%;
				 padding: 10px;
	        }
	        
	        .card_align {
	        	vertical-align: top;
	        }
	        .title {
            margin-left: 10px;
            margin-bottom: 20px;
        }
        .content {
            margin-left: 10px;
        }
	        
	    </style>
	</head>
	<body>
		<%@include file="header.jsp" %>
		<%
		String imported_code = request.getParameter("code");
		if(imported_code==null)
			imported_code="1";
		
		session.setAttribute("codeint", imported_code);
		%>
		
		<%@ include file="display_product_data.jsp" %>
		
		<div class="card mb-3">
		
        <h1 class="title"><%=listProductData.get(0).getProductName()%></h1>
        
        

        <h4 class="content">Photo du produit :</h4>
	    
	    <%if(listProductData.get(0).getImageSmallUrl()==null || listProductData.get(0).getImageSmallUrl().equals("")){%>
			<img alt="product_picture" width="500" height="500" src="foodLogo.png">
		<%} else {%>
			<img alt="product_picture" width="500" height="500" src="<%= listProductData.get(0).getImageSmallUrl()%>">
		<%}%>
	    
	    <ul class="list-group list-group-flush">
	    
        	<li class="list-group-item"><h4>Code du produit :</h4>
	    	<p><%=listProductData.get(0).getCode()%></p></li>
	    
	    	<li class="list-group-item"><h4>Nutriscore du produit :</h4>
	    	<p> <%if(listProductData.get(0).getNutriscoreGrade()==null || listProductData.get(0).getNutriscoreGrade().equals("")){%>
						    	<img alt="product_picture" style="width:100px" src="no_logo.png">
						    <%} else if (listProductData.get(0).getNutriscoreGrade().equals("a")) {%>
						    	<img alt="product_picture" style="width:100px" src="a_logo.png">
						    <%} else if (listProductData.get(0).getNutriscoreGrade().equals("b")) {%>
						    	<img alt="product_picture" style="width:100px" src="b_logo.png">
						    <%} else if (listProductData.get(0).getNutriscoreGrade().equals("c")) {%>
						    	<img alt="product_picture" style="width:100px" src="c_logo.png">
						    <%} else if (listProductData.get(0).getNutriscoreGrade().equals("d")) {%>		
						    	<img alt="product_picture" style="width:100px" src="d_logo.png">				    						   
						    <%} else if (listProductData.get(0).getNutriscoreGrade().equals("e")) {%>
						    	<img alt="product_picture" style="width:100px" src="e_logo.png">
						    <%}%>
	    	
	    	</p></li>
	    
	    	<li class="list-group-item"><h4>Origine du produit :</h4>
	    	<p><%= listProductData.get(0).getCountriesFr()%></p></li>
	    </ul>
	    
	    <br/>
	    
	    
	    <h4 class="content">Autres info :</h4>  
		<ul class="list-group">
			<li class="list-group-item d-flex justify-content-between align-items-center">
				<strong>1. Allergènes</strong> 
				<p> 
				<%
			    
			    if(listProductData.get(0).getAllergens()==null || listProductData.get(0).getAllergens().equals("")){%>
				<strong>Inconnu</strong>
				<%}else {%>
					<%
					String str = listProductData.get(0).getAllergens();
			    	String[] parts = str.split(",");
			    	
			    	for (int i = 0; i < parts.length; i++) {
			    		parts[i] = parts[i].substring(3);
			    	}		

			    	String result = String.join(",", parts);%>
			    
			    	<%=result%>
				<% } %>
				
				
				</p>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				<strong>2. Valeur nutritionelles</strong> 
				<ul>
					<li><p>Energie kcal :
					<%if(listProductData.get(0).getEnergyKcal100g()==null || listProductData.get(0).getEnergyKcal100g().equals("")){%>
						<strong>Inconnu</strong>
						<%}else {%>
							<%=listProductData.get(0).getEnergyKcal100g()%>
						<% } %></p></li>
						
					<li><p>Gras saturé :
					<%if(listProductData.get(0).getSaturatedFat100g()==null || listProductData.get(0).getSaturatedFat100g().equals("")){%>
						<strong>Inconnu</strong>
						<%}else {%>
							<%=listProductData.get(0).getSaturatedFat100g()%>
						<% } %></p></li>
						
					<li><p>Sucre :
					<%if(listProductData.get(0).getSugars100g()==null || listProductData.get(0).getSugars100g().equals("")){%>
						<strong>Inconnu</strong>
						<%}else {%>
							<%=listProductData.get(0).getSugars100g()%>
						<% } %></p></li>
					
					<li><p>Sel :
					<%if(listProductData.get(0).getSalt100g()==null || listProductData.get(0).getSalt100g().equals("")){%>
						<strong>Inconnu</strong>
						<%}else {%>
							<%=listProductData.get(0).getSalt100g()%>
						<% } %></p></li>
					
					<li><p>Protéine :
					<%if(listProductData.get(0).getProteins100g()==null || listProductData.get(0).getProteins100g().equals("")){%>
						<strong>Inconnu</strong>
						<%}else {%>
							<%=listProductData.get(0).getProteins100g()%>
						<% } %></p></li>
					
					<li><p>Fibre :
					<%if(listProductData.get(0).getFiber100g()==null || listProductData.get(0).getFiber100g().equals("")){%>
						<strong>Inconnu</strong>
						<%}else {%>
							<%=listProductData.get(0).getFiber100g()%>
						<% } %></p></li>
					
					<li><p>Sodium :
					<%if(listProductData.get(0).getSodium100g()==null || listProductData.get(0).getSodium100g().equals("")){%>
						<strong>Inconnu</strong>
						<%}else {%>
							<%=listProductData.get(0).getSodium100g()%>
						<% } %></p></li>
					
				</ul>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				<strong>3. Composition</strong> 
			    <p class="text-center"> 
			    <%
			    
			    if(listProductData.get(0).getIngredientsTags()==null || listProductData.get(0).getIngredientsTags().equals("")){%>
				<strong>Inconnu</strong>
				<%}else {%>
					<%
					String str = listProductData.get(0).getIngredientsTags();
			    	String[] parts = str.split(",");
			    	
			    	for (int i = 0; i < parts.length; i++) {
			    		parts[i] = parts[i].substring(3);
			    	}		

			    	String result = String.join(",", parts);%>
			    
			    	<%=result%>
				<% } %>
			    
			    
			    
			    </p>
			</li>
		</ul>
		
		
		<div class="content">
	    <h4>Note moyenne du produit :</h4>
		<%
		// reset values 
		stmt = null;
		sql = null;
		rs = null;
		PreparedStatement ps = null;
		
		 // Préparer et exécuter la requête SQL
	    sql = "SELECT AVG(note) as moyenne FROM rating WHERE id_product = ?";
	    ps = conn.prepareStatement(sql);
	    System.out.print(listProductData.get(0).getId());
	    ps.setInt(1, listProductData.get(0).getId());
	    rs = ps.executeQuery();

	    // Récupérer la moyenne des notes
	    double avg_rating = -1;
	    if (rs.next()) {
	        avg_rating = rs.getDouble("moyenne");
	    }
	    DecimalFormat df = new DecimalFormat("0.00");
	    df.setRoundingMode(RoundingMode.DOWN);
		%>
		<p> <%=df.format(avg_rating)%></p>
	
	    <h4>Vous souhaitez laisser une note ?</h4>
 
	    <div>
	    	<form  method="POST" action="postRating.jsp">
	      	<div class="col-md-2">
	        	<label for="rating" class="form-label">Note</label>
	        		<input type="text" class="form-control " id="rating" name="rating" placeholder="Note / 5" required>
	      		</div>
		    	<div class="row my-3">
		      		<div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Noter</button></div>   
		   		</div>
		   		<input type="hidden" name="code" value="<%= listProductData.get(0).getCode()%>"/> 
  			</form>
		</div>
</div>
<%@include file="footer.jsp" %>
	</body>
</html>