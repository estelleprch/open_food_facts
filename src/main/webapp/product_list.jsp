<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.servlet.http.Cookie" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <title>Liste des produits</title>

    <style>
        
        .card_space {
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        
        .filter_part {
        	width: 10%;
        	float: left;
        	margin-left: 10px;
        }
        
        .list_part {
        	width: 88%;
        	float: right;
        	margin-right: 10px;
        }

        .nav_space {
			margin-top: 10px;
		   	margin-left: auto;
			margin-right: auto;
        }
        
        .card_align {
        	vertical-align: top;
        }
        
        .title {
            margin-left: 10px;
            margin-bottom: 20px;
        }
        
    </style>

</head>
<body>
	<%@include file="header.jsp" %>
	<h1 class="text-black-80 title">Résultat(s) de la recherche</h1>


	<%
		// get all parameters
		
		//get nav
		String nav = request.getParameter("nav");
		if(nav==null  || nav.equals(""))
			nav="1";
		
		//get search
		String search = request.getParameter("search");
		if( search==null || search.equals("") )
			search="";
		
		//get filter
		String filter = request.getParameter("filter");
		if( filter ==null || filter.equals("") )
			filter="";
		
	%>
	
	<%
	//filter part
	%>
	
	<div class="filter_part">
	
			
	    <ul class="list-group">
		  <li class="list-group-item disabled"><strong>Filtres</strong></li>
		  <li class="list-group-item">
		  
		  
		  <form method="get" action="product_list.jsp">
		  	<select class="form-select" name="filter" id="filter">
				<option>Origine</option>
				<%@ include file="filter_origin.jsp" %>
			</select><br>
			<input type="hidden" id="search" name="search" value=<%=search%>>
			<button class="btn btn-outline-success" type="submit">Appliquer</button>
		  </form>
		  
		  
		  </li>
		  
		</ul>
			    
	</div>
	
	
	<div class="list_part card_list">
		<div class="list-group">
			
		<%	//card part
			//DEFINE NUMBER OF VALUES IN A NAV
			int list_limit=50;
			
			//create list
			List<Product> listDisplayedProduct = new ArrayList<Product>();
			
			if( search.equals("") ) {
			%>
				<%@ include file="display_product_list.jsp" %>
			<% } else { %>
				<%@ include file="filter_product.jsp" %>
			<%}
			
			int nbr_list = listDisplayedProduct.size();
			
			//establish nav system
			int navint=Integer.parseInt(nav);
			int cpt_article = listDisplayedProduct.size();
			int cpt_page = listDisplayedProduct.size()/list_limit+1;
			int debut=0;
			int fin=0;
			if (cpt_page != navint) {
				debut=(navint-1)*list_limit;
					fin=debut+list_limit;
			} else {
				debut=(navint-1)*list_limit;
					fin=cpt_article;
			}
				
			//display first line %>
			<a href="" class="list-group-item list-group-item-action disabled">
			<div class="container">
				  <div class="row">
				    <div class="col-md-3 text-center">
				    	<strong>Nom</strong>
				    </div>
				    <div class="col-md-3 text-center">
				      <strong>Code</strong>
				    </div>
				    <div class="col-md-3 text-center">
				    <strong>Nutriscore</strong>
				    </div>
				    <div class="col-md-3 text-center">
						<strong>Photo</strong>
				  	</div>
				  </div>
				</div>
			</a>
			
			<%
			for(int i=debut;i<fin;i++){%>	

				<a href="uniq_product.jsp?code=<%= listDisplayedProduct.get(i).getCode()%>" class="list-group-item list-group-item-action ">
					<div class="container">
					  <div class="row">
					    <div class="col-md-3 text-center">
					      <strong><%=listDisplayedProduct.get(i).getProductName()%></strong>
					    </div>
					    <div class="col-md-3 text-center">
					      <%= listDisplayedProduct.get(i).getCode()%>
					    </div>
					    <div class="col-md-3 text-center">
					    
						    <%if(listDisplayedProduct.get(i).getNutriscoreGrade()==null || listDisplayedProduct.get(i).getNutriscoreGrade().equals("")){%>
						    	<img alt="product_picture" style="width:100px" src="no_logo.png">
						    <%} else if (listDisplayedProduct.get(i).getNutriscoreGrade().equals("a")) {%>
						    	<img alt="product_picture" style="width:100px" src="a_logo.png">
						    <%} else if (listDisplayedProduct.get(i).getNutriscoreGrade().equals("b")) {%>
						    	<img alt="product_picture" style="width:100px" src="b_logo.png">
						    <%} else if (listDisplayedProduct.get(i).getNutriscoreGrade().equals("c")) {%>
						    	<img alt="product_picture" style="width:100px" src="c_logo.png">
						    <%} else if (listDisplayedProduct.get(i).getNutriscoreGrade().equals("d")) {%>		
						    	<img alt="product_picture" style="width:100px" src="d_logo.png">				    						   
						    <%} else if (listDisplayedProduct.get(i).getNutriscoreGrade().equals("e")) {%>
						    	<img alt="product_picture" style="width:100px" src="e_logo.png">
						    <%}%>
						    
					    </div>
					    <div class="col-md-3 text-center">
				
					    <%if(listDisplayedProduct.get(i).getImageSmallUrl()==null || listDisplayedProduct.get(i).getImageSmallUrl().equals("")){%>
					    	<img alt="product_picture" style="width:100px" src="foodLogo.png">
					    <%} else {%>
					    	<img alt="product_picture" width="50" height="50" src="<%= listDisplayedProduct.get(i).getImageSmallUrl()%>">
					    <%}%>
					      
					    </div>
					    
					  </div>
					</div>
				</a>
			
    	<%}%>
	    	
		</div>
		
	    <% 
        //nav part
	    %>
		
		<nav aria-label="Page navigation example" class="nav_space">
			<ul class="pagination">
			
				<li class="page-item">
		          
		          	<%if(navint==1){%>	          	
		          		<a class="page-link disabled" href="product_list.jsp?nav=<%=navint-1%>&search=<%=search%>&filter=<%=filter%>">Previous</a>	          		
	          		<%} else {%>
		          		<a class="page-link" href="product_list.jsp?nav=<%=navint-1%>&filter=<%=filter%>">Previous</a>
		          	<%}%>
		          	
				</li>
				
					<%for (int i=4; i>0; i--){%>  
						<%if (navint-i>0){ %>  
							<li class="page-item"><a class="page-link" href="product_list.jsp?nav=<%=navint-i%>&search=<%=search%>&filter=<%=filter%>"><%=navint-i%></a></li>
						<% } %> 
					<% } %>         
			        <li class="page-item"><a class="page-link" href="product_list.jsp?nav=<%=navint%>&search=<%=search%>&filter=<%=filter%>"><%=navint%></a></li>
			        <%for (int i=1; i<5; i++){%>  
						<%if (navint+i<=cpt_page){ %>  
							<li class="page-item"><a class="page-link" href="product_list.jsp?nav=<%=navint+i%>&search=<%=search%>&filter=<%=filter%>"><%=navint+i%></a></li>
						<% } %> 
					<% } %>      
			          
		          
		          
				<li class="page-item">
					<%if(navint==cpt_page){%>	          	
		          		<a class="page-link disabled" href="product_list.jsp?nav=<%=navint+1%>&search=<%=search%>&filter=<%=filter%>">Next</a>	          		
	          		<%} else {%>
		          		<a class="page-link" href="product_list.jsp?nav=<%=navint+1%>&search=<%=search%>&filter=<%=filter%>">Next</a>
		          	<%}%>
				</li>
				
	        </ul>
	    </nav>
	    <%@include file="footer.jsp" %>
	</div>
	  
</body>
</html>
