<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.servlet.http.Cookie" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <title>Liste des rappels</title>

    <style>
        
        .card_space {
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        
        .list_part {
        	width: auto;
        	margin-right: 10px;
        	margin-left: 10px;
        }

        .nav_space {
        	 margin-top: 10px;
			 width: 100%;
        	 margin-right: 10px;
        	 margin-left: 10px;
        }
        
		.title {
            margin-left: 10px;
            margin-bottom: 20px;
        }
        
        .card_align {
        	vertical-align: top;
        }
        
    </style>

</head>
<body>
<%@include file="header.jsp" %>

    <h1 class="text-black-80 title">Résultat(s) de la recherche</h1>
    
	<div class="list_part card_list">
		<div class="list-group">
			<%//card part
			int list_limit=50;%>
			<%//import data
			String search = request.getParameter("search");
			
			//create list rappel
			List<Rappel> listDisplayedProduct = new ArrayList<Rappel>();
			%>
				<%@ include file="display_rappel_list.jsp" %>
			
			
			<% 
			int nbr_list = listDisplayedRappel.size();
			
			//get Parameter 
			String nav = request.getParameter("nav");
			if(nav==null)
				nav="1";
				
			int navint=Integer.parseInt(nav);
			int cpt_article = listDisplayedRappel.size();
			int cpt_page = listDisplayedRappel.size()/list_limit+1;
			int debut=0;
			int fin=0;
				
			if (cpt_page != navint) {
				debut=(navint-1)*list_limit;
 				fin=debut+list_limit;
			} else {
				debut=(navint-1)*list_limit;
 				fin=cpt_article;
			}
					
			//display first line %>	
				
				<a href="" class="list-group-item list-group-item-action disabled">
					<div class="container">
						<div class="row">
						    <div class="col-md-3 text-center">
						    	<strong>Nom Rappel</strong>
						    </div>
						    <div class="col-md-3 text-center">
						      <strong>Sous catégorie</strong>
						    </div>
						    <div class="col-md-3 text-center">
						    <strong>Code de rappel</strong>
						    </div>
						    <div class="col-md-3 text-center">
								<strong>Photo</strong>
						  	</div>
						</div>
					</div>
				</a>
			
			<%
			for(int i=debut;i<fin;i++){%>	
			<a  href="uniq_rappel.jsp?code=<%= listDisplayedRappel.get(i).getRappelguid()%>" class="list-group-item list-group-item-action">
				<div class="container">
					<div class="row">
					    <div class="col-md-3 text-center">
							<strong><%=listDisplayedRappel.get(i).getNomsDesModelesOuReferences()%></strong><br>
				      	</div>
					    <div class="col-md-3 text-center">
					      <%= listDisplayedRappel.get(i).getSousCategorieDeProduit()%><br>
					    </div>
					    <div class="col-md-3 text-center">
					      <%= listDisplayedRappel.get(i).getRappelguid()%><br>
					    </div>
					    <div class="col-md-3 text-center">
						    <%if(listDisplayedRappel.get(i).getLiensVersLesImages()==null || listDisplayedRappel.get(i).getLiensVersLesImages().equals("")){%>
						    	<img alt="rappel_picture" style="width:100px" src="foodLogo.png">
						    <%} else {%>
						    	<img alt="rappel_picture" width="50" height="50" src="<%= listDisplayedRappel.get(i).getLiensVersLesImages()%>">
						    <%}%>
					    </div>
				  	</div>    
  				</div>
  			</a>
		<%} %>
		</div>
	</div>
	    
	    <% 
        //nav part
	    %>
	    <div class="navbar-nav flex-wrap">
	    
  			<nav aria-label="Page navigation example" class="nav_space">
				<ul class="pagination">
				
					<li class="page-item">
			          
			          	<% if(navint==1){ %>	          	
			          		<a class="page-link disabled" href="rappel_list.jsp?nav=<%=navint-1%>">Previous</a>	          		
		          		<% } else { %>
			          		<a class="page-link" href="rappel_list.jsp?nav=<%=navint-1%>">Previous</a>
			          	<% } %>
			          	
					</li>
					
					
					<%for (int i=4; i>0; i--){%>  
						<%if (navint-i>0){ %>  
							<li class="page-item"><a class="page-link" href="rappel_list.jsp?nav=<%=navint-i%>"><%=navint-i%></a></li>
						<% } %> 
					<% } %>         
			        <li class="page-item"><a class="page-link" href="rappel_list.jsp?nav=<%=navint%>"><%=navint%></a></li>
			        <%for (int i=1; i<5; i++){%>  
						<%if (navint+i<=cpt_page){ %>  
							<li class="page-item"><a class="page-link" href="rappel_list.jsp?nav=<%=navint+i%>"><%=navint+i%></a></li>
						<% } %> 
					<% } %>      
			          
			          
					<li class="page-item">
						<% if(navint==cpt_page) { %>	          	
			          		<a class="page-link disabled" href="rappel_list.jsp?nav=<%= navint+1 %>">Next</a>	          		
		          		<% } else { %>
			          		<a class="page-link" href="rappel_list.jsp?nav=<%= navint+1 %>">Next</a>
			          	<% } %>
					</li>
					
		        </ul>
		    </nav>
	    
		</div>
<%@include file="footer.jsp" %>

</body>
</html>
