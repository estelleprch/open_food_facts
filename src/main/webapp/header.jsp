<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Open Food Facts</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav" style="line-height: 5em">
        <a class="nav-link active" aria-current="page" href="home.jsp">
        <img src="foodLogo.png" class="img-thumbnail" style="width:130px" alt="food logo">
        </a>
        
        <a class="nav-link fs-4" href="product_list.jsp">Liste des produits</a>
        <a class="nav-link fs-4" href="rappel_list.jsp">Rappels</a>
      </div>
    </div>
    <form class="d-flex" role="search" method="get" action="product_list.jsp">
      <input class="form-control me-3 fs-5" type=search id="search" name="search" placeholder="Rechercher" aria-label="Rechercher">
      <button class="btn btn-outline-success" type="submit">Rechercher</button>
    </form>
  </div>
</nav>
</body>

</html>
