<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="off.Product" %>
<%@ page import="java.util.concurrent.ThreadLocalRandom" %>

<%@ include file="connection.jsp" %>

<%
	//Execute a SELECT statement 
	Statement stmt = conn.createStatement();
	String sql;
	ResultSet rs;
		
    // Construire la requ�te SQL
    if (!filter.equals("")) {
        sql = "SELECT * FROM product WHERE product_name LIKE ? AND countries_fr= ? ";
    } else {
        sql = "SELECT * FROM product WHERE product_name LIKE ?";
    }
    
    // Cr�er un objet PreparedStatement pour ex�cuter la requ�te
    PreparedStatement pstmt = conn.prepareStatement(sql);
    if (!filter.equals("")) {
        pstmt.setString(1, "%" + search + "%"); 
        pstmt.setString(2, filter); 
    } else {
        pstmt.setString(1, "%" + search + "%");
    }
    

    		
    // Ex�cuter la requ�te
    rs = pstmt.executeQuery();
      
     // Parcourir les r�sultats et renvoyer vrai si un produit correspondant est trouv�
     while (rs.next()) {
        String name = rs.getString("product_name");
        String code = rs.getString("code");
        String nutriscore = rs.getString("nutriscore_grade");
        String small_image = rs.getString("image_small_url");	
        if (name.contains(name)) {
        	listDisplayedProduct.add(new Product(code, name, nutriscore, small_image));
        }
     }
%>