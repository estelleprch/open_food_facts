<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="off.Product" %>
<%@ include file="connection.jsp" %>

<%
	//create list
	List<Product> listProductData = new ArrayList<Product>();

	//Execute a SELECT statement 
	Statement stmt = conn.createStatement();
	String codeString = session.getAttribute("codeint").toString();

	String sql = "select * from product where code='"+codeString+"'";
	
	ResultSet rs = stmt.executeQuery(sql);
	
	// Process the results and get all valuable data
	while (rs.next()) {
		int id = rs.getInt("id"); 
		String code = rs.getString("code"); 
        String url = rs.getString("url");
        String productName = rs.getString("product_name"); 
        String brands = rs.getString("brands"); 
        String brandsTags = rs.getString("brands_tags"); 
        String categoriesFr = rs.getString("categories_fr"); 
        String countriesFr = rs.getString("countries_fr"); 
        String ingredientsTags = rs.getString("ingredients_tags"); 
        String allergens = rs.getString("allergens"); 
        String additivesFr = rs.getString("additives_fr"); 
        String nutriscoreGrade = rs.getString("nutriscore_grade"); 
        if (nutriscoreGrade==null)
        	nutriscoreGrade="inconnu";
        String foodGroupsFr = rs.getString("food_groups_fr");
        String imageUrl = rs.getString("image_url"); 
        String imageSmallUrl = rs.getString("image_small_url"); 
        String energyKcal100g = rs.getString("energy-kcal_100g"); 
        String energy100g = rs.getString("energy_100g"); 
        String fat100g = rs.getString("fat_100g"); 
        String saturatedFat100g = rs.getString("saturated-fat_100g"); 
        String carbohydrates100g = rs.getString("carbohydrates_100g"); 
        String sugars100g = rs.getString("sugars_100g"); 
        String fiber100g = rs.getString("fiber_100g"); 
        String proteins100g = rs.getString("proteins_100g"); 
        String salt100g = rs.getString("salt_100g"); 
        String sodium100g = rs.getString("sodium_100g");
           
        listProductData.add(new Product(id, code, url, productName, brands, brandsTags, categoriesFr, countriesFr, ingredientsTags, allergens,additivesFr, nutriscoreGrade, foodGroupsFr, imageUrl, imageSmallUrl, energyKcal100g, energy100g ,fat100g, saturatedFat100g, carbohydrates100g, sugars100g, fiber100g, proteins100g, salt100g, sodium100g));
	}
%>